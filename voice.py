from serial import Serial
import gsmmodem
from gsmmodem.serial_comms import SerialComms
import threading
import logging
import time
import sys
import subprocess

VOICEPORT = '/dev/ttyUSB1'
VOICEBAUDRATE=230400
PORT = '/dev/ttyUSB2'
BAUDRATE = '115200'
CALL = None
BUFFERSIZE = 320
TIMEOUT = 1

class VoiceChannel(Serial):
    def __init__(self, voice_port, call, baudrate=VOICEBAUDRATE, source=None,
                 sink=None):
        super(Serial, self).__init__(voice_port, baudrate, dsrdtr=True, rtscts=True, timeout=TIMEOUT)
        logging.debug('started voice serial')
        self.call = call
        self.call._callStatusUpdateCallbackFunc = self.handle_call_update
        self.source = source
        self.sink = sink
        self.enable_voice_channel()
        self.set_and_start_threads()
        self.close()

    def enable_voice_channel(self):
        self.call._gsmModem.write('AT^DDSETEX=2')

    def send_voice(self):
        while self.call.active and self.source:
            self.write(self.source.read(BUFFERSIZE))
        global CALL
        CALL = None
        self.call.hangup()
        print('finished sending')

    def receive_voice(self):
        while self.sink and self.call.active:
            buffer = self.read(BUFFERSIZE)
            if buffer:
                print(buffer)
            else:
                print('timeout'*100)
                call.hangup()
                global CALL
                CALL = None
        print('finished receiving')

    def set_and_start_threads(self):
        self.tx_thread = threading.Thread(target=self.send_voice)
        self.rx_thread = threading.Thread(target=self.receive_voice)
        logging.debug('waiting rx/tx threads')
        self.tx_thread.start()
        self.rx_thread.start()
        self.tx_thread.join()
        self.rx_thread.join()

    def handle_call_update(self, call):
        global CALL
        CALL = None

def call_handler(call):
    global CALL
    print('incoming call from ', end='')
    if not CALL:
        CALL = call
        if call.number:
            print('{}'.format(call.number))
        else:
            print('unknown number')
        call.answer()
        time.sleep(2)
        ffmpeg = subprocess.Popen('ffmpeg -re -f pulse -i default -f s16le -c:a pcm_s16le -af asetrate=44100*2.2 -ar 8000  -'.split(), stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        voice_channel = VoiceChannel(VOICEPORT, call,
                                     source=ffmpeg.stdout)
        ffmpeg.kill()
        call.hangup()

    else:
        print('{} but lock is on'.format(call.number if call.number else 'unknown number'))

if __name__ == '__main__':
    modem = gsmmodem.GsmModem(PORT, BAUDRATE, incomingCallCallbackFunc=call_handler)
    modem.connect()
    print('waiting for call')
    try:
        modem.rxThread.join(2**31)
    finally:
        modem.close()
